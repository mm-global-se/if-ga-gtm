mmcore.IntegrationFactory.register('GA GMT', {
  defaults: {

    event: 'mvt',

    dataStructure: {
      campaignNameProperty: 'campaign',
      currentExperienceProperty: 'experience',
      experienceHistoryProperty: 'history'
    },

    modifiers: {
      sitePersist: function (data) {
        /**
         * modifier configuration
         * Please adapt to match your case
         */
        var config = {
          // allowed amount of characters to be sent to 3d party
          maxDataSize: 120,
          // cookie name for campaign data storage
          cookieName: 'mm-if-site-persist-ga-gtm'
        },

        getCampaignIndex=function(a){var b;for(b=a.length;b--;)if(new RegExp("("+data.campaign+")=").test(a[b]))return b;return-1},concatCampaigns=function(){var a=getFromStorage(config.cookieName),b=a?JSON.parse(a):[],c=getCampaignIndex(b);return-1!==c?b[c]=data.campaignInfo:b.push(data.campaignInfo),b.join("&")},getFromStorage=function(){return mmcore.GetCookie(config.cookieName,1)},saveToStorage=function(a){var b=a.split("&");mmcore.SetCookie(config.cookieName,JSON.stringify(b),365,1)},removeOldestCampaigns=function(a){for(var b=a.split("&");;){if(!(b.join("&").length>=config.maxDataSize))return b.join("&");b.shift()}},updateCampaignInfo=function(a){data.currentCampaignInfo=data.campaignInfo,data.campaignInfo=a},initialize=function(){var a=concatCampaigns();a=removeOldestCampaigns(a),updateCampaignInfo(a),saveToStorage(a)}();
      }   
    },

    isStopOnDocEnd: false
  },

  validate: function (data) {
    if(!data.campaign)
      return 'No campaign.';

    return true;
  },

  check: function (data) {
    return window.dataLayer && typeof window.dataLayer.push;
  },

  exec: function (data)  {
    var mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
        campaign = mode + data.campaign,
        currentCampaignInfo = data.currentCampaignInfo.replace(data.campaign + '=', ''),
        campaignInfo = data.campaignInfo,
        dataObject = {
          event: data.event || 'mvt'
        },

        cnp = data.dataStructure['campaignNameProperty'],
        cep = data.dataStructure['currentExperienceProperty'],
        ehp = data.dataStructure['experienceHistoryProperty'];

    cnp && (dataObject[cnp] = campaign);
    cep && (dataObject[cep] = currentCampaignInfo);
    ehp && (dataObject[ehp] = campaignInfo);

    dataLayer.push(dataObject);

    if (typeof data.callback === 'function') data.callback();
    return true;
  }
});