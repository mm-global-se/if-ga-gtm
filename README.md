# Google Analytics (GTM)

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to Google Analytics via Google Tag Manager (GTM).

## How we send the data

Campaign generation info is being sent to GA via pushing custom event action to the data layer (to get more details about event actions visit [Google Developers](https://developers.google.com/tag-manager/enhanced-ecommerce#product-clicks) site).

## Data Format

Custom event action data structure can vary from client to client, that's why we made it configurable. Please concider example below:

```javascript

dataLayer.push({
  event: 'mvt',
  campaign: 'MM_Prod_T33_Basket',
  experience: 'reduced:Default|button:green',
  history: 'MM_Prod_T33_Basket=reduced:Default|button:green&MM_Prod_T11_Basket=button:red'
});

```

Configurable parts here are:

+ `event` name - 'mvt' by default

+ `campaign` property name - 'campaign' by default

+ `experience` property name - 'experience'

+ `history` property name - 'history' by default

So it could look like this:

```javascript

dataLayer.push({
  event: 'Maxymiser',
  MMCampaignName: 'MM_Prod_T33_Basket',
  MMElementKeysAndValues: 'reduced:Default|button:green',
  MMVisitorCampaigns: 'MM_Prod_T33_Basket=reduced:Default|button:green&MM_Prod_T11_Basket=button:red'
});

```

or like this:

```javascript

dataLayer.push({
  event: 'mm-modif',
  name: 'MM_Prod_T33_Basket',
  condition: 'reduced:Default|button:green'
});

```

__Note:__ `history` property is optional

## Where To Configure?

Data structure can be configurated from the [register script](https://bitbucket.org/mm-global-se/if-ga-gtm/src/master/src/ga-gtm-register.js) changing following code section:

```javascript

// ...

  defaults: {

    event: 'mvt',

    dataStructure: {
      campaignNameProperty: 'campaign',
      currentExperienceProperty: 'experience',
      experienceHistoryProperty: 'history'
    },

// ...

```

## Download

* [ga-gtm-register.js](https://bitbucket.org/mm-global-se/if-ga-gtm/src/master/src/ga-gtm-register.js)

* [ga-gtm-initialize.js](https://bitbucket.org/mm-global-se/if-ga-gtm/src/master/src/ga-gtm-initialize.js)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name

+ Custom event action [data structure](#data-format)

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [ga-gtm-register.js](https://bitbucket.org/mm-global-se/if-ga-gtm/src/master/src/ga-gtm-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [ga-gtm-initialize.js](https://bitbucket.org/mm-global-se/if-ga-gtm/src/master/src/ga-gtm-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

+ Type `window.dataLayer` in chrome dev console

+ Look for campaign related data